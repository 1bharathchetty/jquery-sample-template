function hideParagraphs(){
  $(document).ready(function(){
      $("#hideParas").click(function(){
          $("p").hide();
      });
  });
}

function showParagraphs(){
  $(document).ready(function(){
      $("#showParas").click(function(){
          $("p").show();
      });
  });
}
